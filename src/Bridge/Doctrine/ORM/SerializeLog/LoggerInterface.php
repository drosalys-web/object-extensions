<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\SerializeLog;

use Doctrine\ORM\EntityManagerInterface;
use DrosalysWeb\ObjectExtensions\SerializeLog\LoggerInterface as BaseInterface;

/**
 * Interface LoggerInterface
 *
 * @author Benjamin Georgeault
 */
interface LoggerInterface extends BaseInterface
{
    public function setEm(EntityManagerInterface $em): void;
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Class BlameSubscriber
 *
 * @author Benjamin Georgeault
 */
/** Need fix deprecated now use AsDoctrineListener */
class BlameSubscriber implements EventSubscriber
{
    /**
     * @var string
     */
    private $userEntityClass;

    /**
     * @var string[]
     */
    private $namespaces;

    /**
     * BlameSubscriber constructor.
     * @param string $userEntityClass
     * @param string[] $namespaces Use to apply this mapping only for some entity namespaces.
     */
    public function __construct(string $userEntityClass, array $namespaces = [])
    {
        $this->userEntityClass = $userEntityClass;
        $this->namespaces = $namespaces;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();

        if (false === $mappingAllowed = empty($this->namespaces)) {
            foreach ($this->namespaces as $namespace) {
                if (strstr($reflectionClass->getName(), $namespace)) {
                    $mappingAllowed = true;
                    break;
                }
            }
        }

        if (true === $mappingAllowed) {
            if ($reflectionClass->implementsInterface(CreatedBlameInterface::class)) {
                $this->mappingCreatedBlame($metadata);
            }

            if ($reflectionClass->implementsInterface(UpdatedBlameInterface::class)) {
                $this->mappingUpdatedBlame($metadata);
            }
        }
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function mappingCreatedBlame(ClassMetadata $metadata)
    {
        if (!$metadata->hasAssociation('createdBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'createdBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'onDelete' => 'SET NULL',
                    'referencedColumnName' => 'id',
                ]],
                'fetch' => ClassMetadata::FETCH_EAGER,
            ]);
        }
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function mappingUpdatedBlame(ClassMetadata $metadata)
    {
        if (!$metadata->hasAssociation('updatedBy')) {
            $metadata->mapManyToOne([
                'fieldName' => 'updatedBy',
                'targetEntity' => $this->userEntityClass,
                'joinColumns' => [[
                    'onDelete' => 'SET NULL',
                    'referencedColumnName' => 'id',
                ]],
                'fetch' => ClassMetadata::FETCH_EAGER,
            ]);
        }
    }
}

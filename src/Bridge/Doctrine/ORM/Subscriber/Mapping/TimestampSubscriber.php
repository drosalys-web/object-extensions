<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use DrosalysWeb\ObjectExtensions\Timestamp\Model\CreatedTimestampInterface;
use DrosalysWeb\ObjectExtensions\Timestamp\Model\UpdatedTimestampInterface;

/**
 * Class TimestampSubscriber
 *
 * @author Benjamin Georgeault
 */
/** Need fix deprecated now use AsDoctrineListener */
class TimestampSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(CreatedTimestampInterface::class)) {
            $this->mappingCreatedTimestamp($metadata);
        }

        if ($reflectionClass->implementsInterface(UpdatedTimestampInterface::class)) {
            $this->mappingUpdatedTimestamp($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws MappingException
     */
    private function mappingUpdatedTimestamp(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('updatedAt')) {
            $metadata->mapField([
                'fieldName' => 'updatedAt',
                'type' => 'datetime',
                'nullable' => true,
            ]);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws MappingException
     */
    private function mappingCreatedTimestamp(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('createdAt')) {
            $metadata->mapField([
                'fieldName' => 'createdAt',
                'type' => 'datetime',
            ]);
        }
    }
}

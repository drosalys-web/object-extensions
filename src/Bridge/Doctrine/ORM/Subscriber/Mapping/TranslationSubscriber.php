<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use DrosalysWeb\ObjectExtensions\Translation\Exception\DoubleInterfaceException;
use DrosalysWeb\ObjectExtensions\Translation\Exception\MissingTranslationPrefixException;
use DrosalysWeb\ObjectExtensions\Translation\Model\TranslatableInterface;
use DrosalysWeb\ObjectExtensions\Translation\Model\TranslationInterface;

/**
 * Class TranslationSubscriber
 *
 * @author Benjamin Georgeault
 */
/** Need fix deprecated now use AsDoctrineListener */
class TranslationSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        if ($metadata->isMappedSuperclass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        $both = 0;
        if ($reflectionClass->implementsInterface(TranslatableInterface::class)) {
            $this->mappingTranslatable($metadata, $reflectionClass);
            $both++;
        }

        if ($reflectionClass->implementsInterface(TranslationInterface::class)) {
            if (!preg_match('/\\\\Translation\\\\([^\\\\]+)$/', $reflectionClass->getName())) {
                throw new MissingTranslationPrefixException($reflectionClass->getName());
            }

            $this->mappingTranslation($metadata, $reflectionClass);
            $both++;
        }

        if ($both > 1) {
            throw new DoubleInterfaceException($reflectionClass->getName());
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @param \ReflectionClass $reflectionClass
     * @throws MappingException
     */
    private function mappingTranslatable(ClassMetadata $metadata, \ReflectionClass $reflectionClass)
    {
        if (!$metadata->hasAssociation('translations')) {
            $metadata->mapOneToMany([
                'fieldName' => 'translations',
                'targetEntity' => $reflectionClass->getNamespaceName() . '\\Translation\\' . $reflectionClass->getShortName(),
                'mappedBy' => 'translatable',
                'indexBy' => 'locale',
                'cascade' => ['persist', 'merge', 'remove'],
                'fetch' => ClassMetadata::FETCH_LAZY,
                'orphanRemoval' => true,
            ]);
        }

        if (!$metadata->hasField('fallback')) {
            $metadata->mapField([
                'fieldName' => 'fallback',
                'type' => 'string',
                'length' => 5,
            ]);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @param \ReflectionClass $reflectionClass
     * @throws MappingException
     */
    private function mappingTranslation(ClassMetadata $metadata, \ReflectionClass $reflectionClass)
    {
        $metadata->setTableName($metadata->getTableName().'_translation');

        if (!$metadata->hasField('locale')) {
            $metadata->mapField([
                'fieldName' => 'locale',
                'type' => 'string',
                'length' => 5,
            ]);
        }

        if (!$metadata->hasAssociation('translatable')) {
            $metadata->mapManyToOne([
                'fieldName' => 'translatable',
                'targetEntity' => preg_replace('/\\\\Translation\\\\([^\\\\]+)$/', '\\\\\1', $reflectionClass->getName()),
                'inversedBy' => 'translations',
                'cascade' => ['persist', 'merge'],
                'fetch' => ClassMetadata::FETCH_EAGER,
                'joinColumns' => [[
                    'name' => 'translatable_id',
                    'referencedColumnName' => 'id',
                    'onDelete' => 'CASCADE',
                    'nullable' => false,
                ]],
            ]);
        }

        $metadata->table['uniqueConstraints'][$metadata->getTableName().'_translation_unique'] = [
            'columns' => ['translatable_id', 'locale'],
        ];
    }
}

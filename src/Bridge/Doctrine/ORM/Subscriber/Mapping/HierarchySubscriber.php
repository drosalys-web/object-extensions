<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use DrosalysWeb\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Class HierarchySubscriber
 *
 * @author Benjamin Georgeault
 */
/** Need fix deprecated now use AsDoctrineListener */
class HierarchySubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(HierarchyInterface::class)) {
            $this->mappingHierarchy($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws MappingException
     */
    private function mappingHierarchy(ClassMetadata $metadata)
    {
        $class = $metadata->getReflectionClass()->getName();

        if (!$metadata->hasAssociation('parent')) {
            $metadata->mapManyToOne([
                'fieldName' => 'parent',
                'targetEntity' => $class,
                'inversedBy' => 'children',
                'joinColumns' => [[
                    'onDelete' => 'SET NULL',
                ]],
            ]);
        }

        if (!$metadata->hasAssociation('children')) {
            $metadata->mapOneToMany([
                'fieldName' => 'children',
                'targetEntity' => $class,
                'mappedBy' => 'parent',
            ]);
        }
    }
}

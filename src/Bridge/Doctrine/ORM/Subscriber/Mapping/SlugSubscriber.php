<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber\Mapping;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use DrosalysWeb\ObjectExtensions\Slug\Model\HierarchySlugInterface;
use DrosalysWeb\ObjectExtensions\Slug\Model\SlugInterface;

/**
 * Class SlugSubscriber
 *
 * @author Benjamin Georgeault
 */
/** Need fix deprecated now use AsDoctrineListener */
class SlugSubscriber implements EventSubscriber
{
    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if (null === $metadata->reflClass) {
            return;
        }

        $reflectionClass = $metadata->getReflectionClass();
        if ($reflectionClass->implementsInterface(SlugInterface::class)) {
            $this->mappingSlug($metadata);
        }
        
        if ($reflectionClass->implementsInterface(HierarchySlugInterface::class)) {
            $this->mappingHierarchySlug($metadata);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws MappingException
     */
    private function mappingSlug(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('slug')) {
            $metadata->mapField([
                'fieldName' => 'slug',
                'type' => 'string',
                'length' => 255,
            ]);
        }
    }

    /**
     * @param ClassMetadata $metadata
     * @throws MappingException
     */
    private function mappingHierarchySlug(ClassMetadata $metadata)
    {
        if (!$metadata->hasField('hierarchySlug')) {
            $metadata->mapField([
                'fieldName' => 'hierarchySlug',
                'type' => 'string',
                'length' => 4095,
            ]);
        }
    }
}

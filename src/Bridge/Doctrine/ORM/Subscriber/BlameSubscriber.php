<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use DrosalysWeb\ObjectExtensions\Blame\BlamerInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Class BlameSubscriber
 *
 * @author Benjamin Georgeault
 */
class BlameSubscriber implements EventSubscriber
{
    /**
     * @var BlamerInterface
     */
    private $blamer;

    /**
     * BlameSubscriber constructor.
     * @param BlamerInterface $blamer
     */
    public function __construct(BlamerInterface $blamer)
    {
        $this->blamer = $blamer;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param PrePersistEventArgs $event
     */
    public function prePersist(PrePersistEventArgs $event)
    {
        $entity = $event->getObject();

        if ($entity instanceof CreatedBlameInterface) {
            if ($this->blamer->support(get_class($entity))) {
                $uow = $event->getObjectManager()->getUnitOfWork();
                $oldUser = $entity->getCreatedBy();
                $this->blamer->applyCreatedBy($entity);

                if ($oldUser !== $user = $entity->getCreatedBy()) {
                    $uow->propertyChanged($entity, 'createdBy', $oldUser, $user);
                    $uow->scheduleExtraUpdate($entity, [
                        'createdBy' => [$oldUser, $user],
                    ]);
                }
            }
        }
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $entity = $event->getObject();

        if ($entity instanceof UpdatedBlameInterface) {
            if ($this->blamer->support(get_class($entity))) {
                $uow = $event->getObjectManager()->getUnitOfWork();
                $oldUser = $entity->getUpdatedBy();
                $this->blamer->applyUpdatedBy($entity);

                if ($oldUser !== $user = $entity->getUpdatedBy()) {
                    $uow->propertyChanged($entity, 'updatedBy', $oldUser, $user);
                    $uow->scheduleExtraUpdate($entity, [
                        'updatedBy' => [$oldUser, $user],
                    ]);
                }
            }
        }
    }
}

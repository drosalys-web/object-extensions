<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use DrosalysWeb\ObjectExtensions\Slug\Model\SlugInterface;
use DrosalysWeb\ObjectExtensions\Slug\Slugger;
use DrosalysWeb\ObjectExtensions\Slug\SluggerInterface;

/**
 * Class SlugSubscriber
 *
 * @author Benjamin Georgeault
 */
class SlugSubscriber implements EventSubscriber
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var bool
     */
    private $onPreUpdate;

    /**
     * SlugSubscriber constructor.
     * @param SluggerInterface $slugger
     * @param bool $onPreUpdate
     */
    public function __construct(SluggerInterface $slugger = null, bool $onPreUpdate = true)
    {
        $this->slugger = $slugger ? : new Slugger();
        $this->onPreUpdate = $onPreUpdate;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        $events = [
            Events::prePersist,
        ];

        if ($this->onPreUpdate) {
            $events[] = Events::preUpdate;
        }

        return $events;
    }

    /**
     * @param PrePersistEventArgs $event
     */
    public function prePersist(PrePersistEventArgs $event)
    {
        $this->generateSlug($event);
    }

    /**
     * @param PreUpdateEventArgs $event
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $this->generateSlug($event);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    private function generateSlug(LifecycleEventArgs $event)
    {
        $entity = $event->getObject();

        if ($entity instanceof SlugInterface) {
            $this->slugger->generateSlug($entity);
        }
    }
}

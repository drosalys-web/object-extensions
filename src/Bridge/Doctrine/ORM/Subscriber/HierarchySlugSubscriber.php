<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine\ORM\Subscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Events;
use DrosalysWeb\ObjectExtensions\Slug\HierarchySlugger;
use DrosalysWeb\ObjectExtensions\Slug\HierarchySluggerInterface;
use DrosalysWeb\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Class HierarchySlugSubscriber
 *
 * @author Benjamin Georgeault
 */
class HierarchySlugSubscriber implements EventSubscriber
{
    /**
     * @var HierarchySluggerInterface
     */
    private $slugger;

    /**
     * HierarchySlugSubscriber constructor.
     * @param HierarchySluggerInterface|null $slugger
     */
    public function __construct(HierarchySluggerInterface $slugger = null)
    {
        $this->slugger = $slugger ? : new HierarchySlugger();
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::preFlush,
        ];
    }

    /**
     * @param PreFlushEventArgs $event
     */
    public function preFlush(PreFlushEventArgs $event)
    {
        $em = $event->getObjectManager();
        $uow = $em->getUnitOfWork();

        $entities = array_merge($uow->getScheduledEntityInsertions(), $uow->getScheduledEntityUpdates());

        foreach ($entities as $entity) {
            if ($entity instanceof HierarchySlugInterface) {
                $this->slugger->generateHierarchySlug($entity);
                $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }
        }
    }
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DrosalysWeb\ObjectExtensions\Hierarchy\Model\HierarchyInterface;

/**
 * Trait HierarchyTrait
 *
 * @author Benjamin Georgeault
 * @see HierarchyInterface
 */
trait HierarchyTrait // implements HierarchyInterface
{
    /**
     * @var HierarchyInterface|null
     */
    protected $parent;

    /**
     * @var HierarchyInterface[]|Collection
     */
    protected $children;

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return $this->getChildren()->getIterator();
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return $this->getChildren()->count();
    }

    /**
     * @inheritdoc
     */
    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    /**
     * @return HierarchyInterface[]|Collection
     */
    public function getChildren()
    {
        return $this->children ? : $this->children = new ArrayCollection();
    }

    /**
     * @inheritdoc
     */
    public function hasChild(HierarchyInterface $child): bool
    {
        return $this->getChildren()->contains($child);
    }
}

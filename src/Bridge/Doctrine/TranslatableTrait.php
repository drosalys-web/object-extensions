<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DrosalysWeb\ObjectExtensions\Translation\Exception\AlreadyExistTranslationException;
use DrosalysWeb\ObjectExtensions\Translation\Exception\NotFoundTranslationException;
use DrosalysWeb\ObjectExtensions\Translation\Model\TranslationInterface;

/**
 * Trait TranslatableTrait
 *
 * For usage with
 *
 * @author Benjamin Georgeault
 * @see \DrosalysWeb\ObjectExtensions\Translation\Model\TranslatableInterface
 */
trait TranslatableTrait // implements TranslatableInterface
{
    /**
     * @var TranslationInterface[]|Collection
     */
    protected $translations;

    /**
     * @var string|null
     */
    protected $fallback;

    /**
     * @return TranslationInterface[]|Collection
     */
    public function getTranslations()
    {
        return $this->translations ? : $this->translations = new ArrayCollection();
    }

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return TranslationInterface
     * @throws NotFoundTranslationException
     */
    public function getTranslation(string $locale, string $fallback = null): TranslationInterface
    {
        if ($this->hasTranslation($locale)) {
            return $this->getTranslations()->get($locale);
        }

        if (null === $fallback) {
            $fallback = $this->getFallback();
        }

        if (null !== $fallback && $this->hasTranslation($fallback)) {
            return $this->getTranslations()->get($fallback);
        }

        throw new NotFoundTranslationException($this, $locale);
    }

    /**
     * @param string $locale
     * @param string|null $fallback
     * @return null|TranslationInterface
     */
    public function getTranslationOrNull(string $locale, string $fallback = null): ?TranslationInterface
    {
        try {
            return $this->getTranslation($locale, $fallback);
        } catch (NotFoundTranslationException $e) {
            return null;
        }
    }

    /**
     * @param string $locale
     * @return bool
     */
    public function hasTranslation(string $locale): bool
    {
        return $this->getTranslations()->containsKey($locale);
    }

    /**
     * @param TranslationInterface $translation
     * @return $this
     * @throws AlreadyExistTranslationException
     */
    public function addTranslation(TranslationInterface $translation)
    {
        if ($this->hasTranslation($locale = $translation->getLocale())) {
            throw new AlreadyExistTranslationException($this, $locale);
        }

        $translation->setTranslatable($this);
        $this->getTranslations()->set($locale, $translation);

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFallback(): ?string
    {
        return $this->fallback;
    }

    /**
     * @param string $fallback
     * @return $this
     */
    public function setFallback(string $fallback)
    {
        $this->fallback = $fallback;

        return $this;
    }
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony;

use DrosalysWeb\ObjectExtensions\Blame\BlamerInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\UpdatedBlameInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LazyChainBlamer
 *
 * @author Benjamin Georgeault
 */
class LazyChainBlamer implements BlamerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var string[]
     */
    private $blamerIds;

    /**
     * @var BlamerInterface[]
     */
    private $blamers;

    /**
     * LazyChainBlamer constructor.
     * @param ContainerInterface $container
     * @param string[] $blamerIds
     */
    public function __construct(ContainerInterface $container, array $blamerIds)
    {
        $this->container = $container;
        $this->blamerIds = $blamerIds;
        $this->blamers = [];
    }

    /**
     * @inheritDoc
     */
    public function applyCreatedBy(CreatedBlameInterface $blameObject): void
    {
        if (null !== $blamer = $this->getBlamer(get_class($blameObject))) {
            $blamer->applyCreatedBy($blameObject);
        }
    }

    /**
     * @inheritDoc
     */
    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void
    {
        if (null !== $blamer = $this->getBlamer(get_class($blameObject))) {
            $blamer->applyUpdatedBy($blameObject);
        }
    }

    /**
     * @inheritDoc
     */
    public function support(string $blameClass): bool
    {
        return null !== $this->getBlamer($blameClass);
    }

    /**
     * @param string $class
     * @return BlamerInterface|null
     */
    private function getBlamer(string $class): ?BlamerInterface
    {
        foreach ($this->blamers as $blamer) {
            if ($blamer->support($class)) {
                return $blamer;
            }
        }

        // Lazy loading Blamers.
        while (null !== $id = array_shift($this->blamerIds)) {
            $blamer = $this->container->get($id);

            if (!$blamer instanceof BlamerInterface) {
                throw new \InvalidArgumentException(sprintf(
                    'The service "%s" is not an instance of "%s".',
                    $id,
                    BlamerInterface::class
                ));
            }

            if ($blamer instanceof LazyChainBlamer) {
                throw new \InvalidArgumentException(sprintf(
                    'The service "%s" is a "%s" instance, it cannot be add to another instance of "%s".',
                    $id,
                    LazyChainBlamer::class,
                    LazyChainBlamer::class
                ));
            }

            $this->blamers[] = $blamer;
            if ($blamer->support($class)) {
                return $blamer;
            }
        }

        return null;
    }
}

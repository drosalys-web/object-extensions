<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony;

use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DoctrineBlamer
 *
 * @author Benjamin Georgeault
 */
class DoctrineBlamer extends Blamer
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * DoctrineBlamer constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param ObjectManager $om
     * @param string $userClass
     * @param array $namespaces
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        $om,
        string $userClass,
        array $namespaces
    ) {
        parent::__construct($tokenStorage, $userClass, $namespaces);

        // New interface since doctrine/persistence 1.3 (old removed on 2.0).
        if (interface_exists('Doctrine\Persistence\ObjectManager') && !$om instanceof ObjectManager) {
            throw new \InvalidArgumentException(sprintf('Second argument has to be an instance of %s.', ObjectManager::class));
        }

        $this->om = $om;
    }

    /**
     * @inheritDoc
     */
    public function support(string $blameClass): bool
    {
        return parent::support($this->cleanClass($blameClass));
    }

    /**
     * Get real class name when Doctrine Proxy is used.
     *
     * @param string $class
     * @return string
     */
    private function cleanClass(string $class): string
    {
        try {
            return $this->om->getClassMetadata($class)->getName();
        } catch (MappingException $e) {
            return $class;
        }
    }
}

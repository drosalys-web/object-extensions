<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony;

use DrosalysWeb\ObjectExtensions\Blame\BlamerInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\UpdatedBlameInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class Blamer
 *
 * @author Benjamin Georgeault
 */
class Blamer implements BlamerInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $userClass;

    /**
     * @var string[]
     */
    private $namespaces;

    /**
     * Blamer constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param string $userClass
     * @param array $namespaces
     */
    public function __construct(TokenStorageInterface $tokenStorage, string $userClass, array $namespaces)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userClass = $userClass;
        $this->namespaces = $namespaces;
    }

    /**
     * @inheritDoc
     */
    public function applyCreatedBy(CreatedBlameInterface $blameObject): void
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setCreatedBy($user);
        }
    }

    /**
     * @inheritDoc
     */
    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void
    {
        if (null !== $user = $this->getUser()) {
            $blameObject->setUpdatedBy($user);
        }
    }

    /**
     * @inheritDoc
     */
    public function support(string $blameClass): bool
    {
        if (null !== $this->getUser()) {
            foreach ($this->namespaces as $namespace) {
                if (strstr($blameClass, $namespace)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return object|null
     */
    private function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        $user = $token->getUser();
        if ($user instanceof $this->userClass) {
            return $user;
        }

        return null;
    }
}

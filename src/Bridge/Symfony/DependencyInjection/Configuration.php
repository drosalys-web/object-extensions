<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony\DependencyInjection;

use DrosalysWeb\ObjectExtensions\SerializeLog\Model\State;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Benjamin Georgeault
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritdoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('drosalys_object_extensions');

        $treeBuilder->getRootNode()
            ->children()
                ->append($this->blameNode())
                ->append($this->hierarchyNode())
                ->append($this->slugNode())
                ->append($this->timestampNode())
                ->append($this->translationNode())
                ->append($this->serializeLogNode())
                ->append($this->manipulatorNode())
            ->end()
        ;

        return $treeBuilder;
    }

    /**
     * @return NodeDefinition
     */
    private function blameNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('blame');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->arrayNode('users')
                    ->useAttributeAsKey('class')
                    ->arrayPrototype()
                        ->beforeNormalization()->castToArray()->end()
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function hierarchyNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('hierarchy');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function slugNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('slug');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
            ->children()
                ->scalarNode('delimiter')
                    ->cannotBeEmpty()
                    ->defaultValue('-')
                ->end()
                ->booleanNode('entropy')
                    ->defaultFalse()
                ->end()
                ->booleanNode('on_pre_update')
                    ->defaultFalse()
                ->end()
                ->scalarNode('hierarchy_delimiter')
                    ->cannotBeEmpty()
                    ->defaultValue('/')
                ->end()
            ->end()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function timestampNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('timestamp');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function translationNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('translation');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function serializeLogNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('serialize_log');

        $node = $treeBuilder->getRootNode()
            ->canBeEnabled()
            ->children()
                ->enumNode('serializer')
                    ->values(['symfony', 'jms'])
                    ->defaultValue('jms')
                ->end()
                ->scalarNode('state_class')
                    ->cannotBeEmpty()
                    ->defaultValue(State::class)
                ->end()
                ->scalarNode('model_manager_name')
                    ->cannotBeEmpty()
                    ->defaultValue('default')
                ->end()
            ->end()
        ;

        return $node;
    }

    /**
     * @return NodeDefinition
     */
    private function manipulatorNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('manipulator');

        $node = $treeBuilder->getRootNode()
            ->canBeDisabled()
        ;

        return $node;
    }
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony\DependencyInjection\Compiler;

use DrosalysWeb\ObjectExtensions\Bridge\Symfony\LazyChainBlamer;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class BlamerPass
 *
 * @author Benjamin Georgeault
 */
class BlamerPass implements CompilerPassInterface
{
    /**
     * @inheritDoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(LazyChainBlamer::class)) {
            return;
        }

        $chainBlamerDefinition = $container->getDefinition(LazyChainBlamer::class);
        $blamerIds = $chainBlamerDefinition->getArgument(1);

        // or processing tagged services:
        foreach ($container->findTaggedServiceIds('drosalys_object_extensions.blamer') as $id => $tags) {
            $blamerIds[] = $id;
        }

        $chainBlamerDefinition->setArgument(1, $blamerIds);
    }
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\Symfony;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use DrosalysWeb\ObjectExtensions\Bridge\Symfony\DependencyInjection\Compiler\BlamerPass;
use DrosalysWeb\ObjectExtensions\Bridge\Symfony\DependencyInjection\DrosalysWebObjectExtensionsExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DrosalysWebObjectExtensionsBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebObjectExtensionsBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new BlamerPass());

        if (class_exists('Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass')) {
            $container->addCompilerPass(DoctrineOrmMappingsPass::createXmlMappingDriver([
                realpath(__DIR__.'/../Doctrine/ORM/SerializeLog/mapping') => 'DrosalysWeb\ObjectExtensions\SerializeLog\Model',
            ], ['drosalys_object_extensions.serialize_log.model_manager_name'], 'drosalys_object_extensions.serialize_log.use_internal_entity'));
        }
    }

    /**
     * @inheritDoc
     */
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new DrosalysWebObjectExtensionsExtension();
    }
}

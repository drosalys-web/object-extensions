<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Bridge\JmsSerializer;

use DrosalysWeb\ObjectExtensions\SerializeLog\SerializerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;

/**
 * Class JmsSerializer
 *
 * @author Benjamin Georgeault
 */
class JmsSerializer implements SerializerInterface
{
    /**
     * @var JmsSerializerInterface
     */
    private $jmsSerializer;

    public function __construct(JmsSerializerInterface $jmsSerializer)
    {
        $this->jmsSerializer = $jmsSerializer;
    }

    public function serialize($object, array $groups = []): array
    {
        $context = new SerializationContext();
        $context->setGroups($groups);

        return json_decode(
            $this->jmsSerializer->serialize($object, 'json', $context),
            true
        );
    }
}

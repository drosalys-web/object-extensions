<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug\Model;

/**
 * Trait HierarchySlugTrait
 *
 * @author Benjamin Georgeault
 * @see HierarchySlugInterface
 * @see \DrosalysWeb\ObjectExtensions\Hierarchy\Model\HierarchyInterface
 */
trait HierarchySlugTrait // implements HierarchySlugInterface, \DrosalysWeb\ObjectExtensions\Hierarchy\Model\HierarchyInterface
{
    use SlugTrait;

    /**
     * @var string
     */
    protected string $hierarchySlug = '';

    /**
     * @inheritdoc
     */
    public function getHierarchySlug(): string
    {
        return $this->hierarchySlug;
    }

    /**
     * @inheritdoc
     */
    public function setHierarchySlug(string $hierarchySlug)
    {
        $this->hierarchySlug = $hierarchySlug;

        return $this;
    }
}

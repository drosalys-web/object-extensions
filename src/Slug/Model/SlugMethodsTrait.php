<?php

namespace DrosalysWeb\ObjectExtensions\Slug\Model;

/**
 * Trait SlugMethodTrait
 *
 * @author Kévin Tourret
 * @see SlugInterface
 */
trait SlugMethodsTrait
{

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;

        return $this;
    }

}
<?php

namespace DrosalysWeb\ObjectExtensions\Slug\Model;

/**
 * Trait SlugPropertyTrait
 *
 * @author Kévin Tourret
 * @see SlugInterface
 */
trait SlugPropertyTrait
{

    /**
     * @var string
     */
    protected string $slug = '';

}
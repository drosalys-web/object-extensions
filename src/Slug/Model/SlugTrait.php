<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug\Model;

/**
 * Trait SlugTrait
 *
 * @author Benjamin Georgeault
 * @see SlugInterface
 */
trait SlugTrait // implements SlugInterface
{
    use SlugPropertyTrait;
    use SlugMethodsTrait;
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug\Model;

/**
 * Interface SlugInterface
 *
 * @author Benjamin Georgeault
 */
interface SlugInterface
{
    /**
     * @return string
     */
    public function getSlug(): string;

    /**
     * @param string $slug
     * @return $this
     */
    public function setSlug(string $slug);

    /**
     * @return string[]
     */
    public static function getSlugFields(): array;
}

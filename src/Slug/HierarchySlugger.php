<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug;

use DrosalysWeb\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Class HierarchySlugger
 *
 * @author Benjamin Georgeault
 */
class HierarchySlugger implements HierarchySluggerInterface
{
    /**
     * @var string
     */
    private $delimiter;

    /**
     * HierarchySlugger constructor.
     * @param string $delimiter
     */
    public function __construct(string $delimiter = '/')
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @inheritdoc
     */
    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject)
    {
        $slug = '';
        $current = $hierarchySlugObject;

        do {
            $slug = $current->getSlug().$this->delimiter.$slug;
        } while (null !== $current = $current->getParent());

        $hierarchySlugObject->setHierarchySlug('/'.trim($slug, '/'));
    }
}

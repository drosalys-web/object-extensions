<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug;

use DrosalysWeb\ObjectExtensions\Slug\Exception\SlugException;
use DrosalysWeb\ObjectExtensions\Slug\Model\SlugInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class Slugger
 *
 * @author Benjamin Georgeault
 */
class Slugger implements SluggerInterface
{
    /**
     * @var string
     */
    private $delimiter;

    /**
     * @var null|PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * @var bool
     */
    private $entropy;

    /**
     * Slugger constructor.
     * @param string $delimiter
     * @param bool $entropy
     */
    public function __construct(string $delimiter = '-', bool $entropy = false)
    {
        $this->delimiter = $delimiter;
        $this->entropy = $entropy;
    }

    /**
     * @inheritDoc
     */
    public function generateSlug(SlugInterface $slugObject)
    {
        $fields = $slugObject::getSlugFields();

        $propertyAccessor = $this->getPropertyAccessor();

        $values = [];
        foreach ($fields as $field) {
            if (null !== $value = $propertyAccessor->getValue($slugObject, $field)) {
                $values[] = $this->normalize($value);
            }
        }

        if (empty($values)) {
            throw new SlugException(sprintf(
                'All values used to generate "%s"\'s slug are empty ("%s").',
                get_class($slugObject),
                json_encode($fields)
            ));
        }

        if ($this->entropy) {
            $values[] = substr(uniqid(), 0, 5);
        }

        $slugObject->setSlug(implode($this->delimiter, $values));
    }

    /**
     * @return PropertyAccessor
     */
    private function getPropertyAccessor(): PropertyAccessor
    {
        return $this->propertyAccessor ? : $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param string $value
     * @return string
     */
    private function normalize(string $value): string
    {
        $value = str_replace([
            'à', 'á', 'â', 'ã', 'ä',
            'ç',
            'è', 'é', 'ê', 'ë',
            'ì', 'í', 'î', 'ï',
            'ñ',
            'ò', 'ó', 'ô', 'õ', 'ö',
            'ù', 'ú', 'û', 'ü',
            'ý', 'ÿ',
            'À', 'Á', 'Â', 'Ã', 'Ä',
            'Ç',
            'È', 'É', 'Ê', 'Ë',
            'Ì', 'Í', 'Î', 'Ï',
            'Ñ',
            'Ò', 'Ó', 'Ô', 'Õ', 'Ö',
            'Ù', 'Ú', 'Û', 'Ü',
            'Ý',
        ], [
            'a', 'a', 'a', 'a', 'a',
            'c',
            'e', 'e', 'e', 'e',
            'i', 'i', 'i', 'i',
            'n',
            'o', 'o', 'o', 'o', 'o',
            'u', 'u', 'u', 'u',
            'y', 'y',
            'A', 'A', 'A', 'A', 'A',
            'C',
            'E', 'E', 'E', 'E',
            'I', 'I', 'I', 'I',
            'N',
            'O', 'O', 'O', 'O', 'O',
            'U', 'U', 'U', 'U',
            'Y',
        ], $value);
        $value = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $value);
        $value = str_replace(['/', '_', '|', '+', ' ', '-'], $this->delimiter, $value);

        return $value;
    }
}

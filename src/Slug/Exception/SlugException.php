<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug\Exception;

/**
 * Class SlugException
 *
 * @author Benjamin Georgeault
 */
class SlugException extends \RuntimeException
{
}

<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Slug;

use DrosalysWeb\ObjectExtensions\Slug\Exception\SlugException;
use DrosalysWeb\ObjectExtensions\Slug\Model\HierarchySlugInterface;

/**
 * Interface HierarchySluggerInterface
 *
 * @author Benjamin Georgeault
 */
interface HierarchySluggerInterface
{
    /**
     * @param HierarchySlugInterface $hierarchySlugObject
     * @throws SlugException
     */
    public function generateHierarchySlug(HierarchySlugInterface $hierarchySlugObject);
}

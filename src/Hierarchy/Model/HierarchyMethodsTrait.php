<?php

namespace DrosalysWeb\ObjectExtensions\Hierarchy\Model;

/**
 * Trait HierarchyMethodsTrait
 *
 * @author Kévin Tourret
 */
trait HierarchyMethodsTrait
{

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->children);
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->children);
    }

    /**
     * @inheritdoc
     */
    public function getParent(): ?HierarchyInterface
    {
        return $this->parent;
    }

    /**
     * @inheritdoc
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @inheritdoc
     */
    public function hasChild(HierarchyInterface $child): bool
    {
        return in_array($child, $this->children);
    }
}
<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Hierarchy\Model;

/**
 * Interface EditableHierarchyInterface
 *
 * @author Benjamin Georgeault
 */
interface EditableHierarchyInterface extends HierarchyInterface
{
    /**
     * @param HierarchyInterface|null $parent
     * @return HierarchyInterface
     */
    public function setParent(?HierarchyInterface $parent): HierarchyInterface;

    /**
     * @param HierarchyInterface $child
     * @return HierarchyInterface
     */
    public function addChild(HierarchyInterface $child): HierarchyInterface;

    /**
     * @param HierarchyInterface $child
     * @return HierarchyInterface
     */
    public function remove(HierarchyInterface $child): HierarchyInterface;
}

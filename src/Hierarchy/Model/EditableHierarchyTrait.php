<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Hierarchy\Model;

/**
 * Trait EditableHierarchyTrait
 *
 * @author Benjamin Georgeault
 * @see EditableHierarchyInterface
 */
trait EditableHierarchyTrait // implements EditableHierarchyInterface
{
    use HierarchyTrait;

    /**
     * @inheritdoc
     */
    public function setParent(?HierarchyInterface $parent): HierarchyInterface
    {
        $this->parent = $parent;

        if (null !== $parent && !$parent->hasChild($this) && $parent instanceof EditableHierarchyInterface) {
            $parent->addChild($this);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addChild(HierarchyInterface $child): HierarchyInterface
    {
        if ($this->hasChild($child)) {
            throw new \InvalidArgumentException(sprintf('Child already in the collection.'));
        }

        $this->children[] = $child;

        if ($this !== $child->getParent() && $child instanceof EditableHierarchyInterface) {
            $child->setParent($this);
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function remove(HierarchyInterface $child): HierarchyInterface
    {
        if (false === $key = array_search($child, $this->children)) {
            throw new \OutOfBoundsException(sprintf('Child not found in the collection.'));
        }

        unset($this->children[$key]);

        return $this;
    }
}

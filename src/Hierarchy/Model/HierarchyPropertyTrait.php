<?php

namespace DrosalysWeb\ObjectExtensions\Hierarchy\Model;

/**
 * Trait HierarchyPropertyTrait
 *
 * @author Kévin Tourret
 */
trait HierarchyPropertyTrait
{
    /**
     * @var HierarchyInterface|null
     */
    protected $parent;

    /**
     * @var HierarchyInterface[]
     */
    protected $children = [];
}
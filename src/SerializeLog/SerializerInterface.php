<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\SerializeLog;

/**
 * Interface SerializerInterface
 *
 * @author Benjamin Georgeault
 */
interface SerializerInterface
{
    public function serialize($object, array $groups = []): array;
}

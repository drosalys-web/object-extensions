<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\SerializeLog;

use DrosalysWeb\ObjectExtensions\SerializeLog\Model\SerializeLogInterface;
use DrosalysWeb\ObjectExtensions\SerializeLog\Model\State;
use DrosalysWeb\ObjectExtensions\SerializeLog\Model\StateInterface;

/**
 * Class AbstractLogger
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractLogger implements LoggerInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ContextLoaderInterface|null
     */
    private $contextLoader;

    /**
     * @var string
     */
    private $stateClass;

    public function __construct(SerializerInterface $serializer, string $stateClass = State::class, ContextLoaderInterface $contextLoader = null)
    {
        $this->serializer = $serializer;
        $this->stateClass = $stateClass;
        $this->contextLoader = $contextLoader;
    }

    public function log(SerializeLogInterface $object, string $message = ''): StateInterface
    {
        return $this->doState($object, $message);
    }

    abstract protected function getClass(SerializeLogInterface $object): string;

    abstract protected function getIdentifier(SerializeLogInterface $object);

    private function doState(SerializeLogInterface $object, string $message): StateInterface
    {
        $class = $this->stateClass;
        /** @var StateInterface $state */
        $state = new $class();

        $state
            ->setClass($this->getClass($object))
            ->setIdentifier($this->getIdentifier($object))
            ->setMessage($message)
            ->setData($this->serializer->serialize($object, $object::getSerializeLogGroups()))
        ;

        if ($this->contextLoader) {
            $state->setContext($this->contextLoader->getContext());
        }

        return $state;
    }
}

<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\SerializeLog\Model;

use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameTrait;
use DrosalysWeb\ObjectExtensions\Timestamp\Model\CreatedTimestampTrait;

/**
 * Class SerializeLogState
 *
 * @author Benjamin Georgeault
 */
class State implements StateInterface
{
    use CreatedBlameTrait;
    use CreatedTimestampTrait;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $class;

    /**
     * @var mixed
     */
    private $identifier;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $data;

    /**
     * @var array|null
     */
    private $context;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function setClass(string $class): StateInterface
    {
        $this->class = $class;
        return $this;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier): StateInterface
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): StateInterface
    {
        $this->message = $message;
        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): StateInterface
    {
        $this->data = $data;
        return $this;
    }

    public function getContext(): ?array
    {
        return $this->context;
    }

    public function setContext(?array $context): StateInterface
    {
        $this->context = $context;
        return $this;
    }
}

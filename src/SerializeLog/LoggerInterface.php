<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\SerializeLog;

use DrosalysWeb\ObjectExtensions\SerializeLog\Model\SerializeLogInterface;
use DrosalysWeb\ObjectExtensions\SerializeLog\Model\StateInterface;

/**
 * Interface LoggerInterface
 *
 * @author Benjamin Georgeault
 */
interface LoggerInterface
{
    public function log(SerializeLogInterface $object, string $message = ''): StateInterface;
}

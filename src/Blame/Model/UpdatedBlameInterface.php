<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Interface UpdatedBlameInterface
 *
 * @author Benjamin Georgeault
 */
interface UpdatedBlameInterface
{
    /**
     * @return object|null
     */
    public function getUpdatedBy();

    /**
     * @param object $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy);
}

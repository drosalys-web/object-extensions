<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Interface CreatedBlameInterface
 *
 * @author Benjamin Georgeault
 */
interface CreatedBlameInterface
{
    /**
     * @return object|null
     */
    public function getCreatedBy();

    /**
     * @param object $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy);
}

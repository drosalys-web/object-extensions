<?php

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Trait CreatedBlamePropertyTrait
 *
 * @author Kévin Tourret
 */
trait CreatedBlamePropertyTrait
{
    /**
     * @var object|null
     */
    protected $createdBy;
}
<?php

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Trait UpdatedBlameMethodsTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedBlameMethodsTrait
{

    /**
     * @return object|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param object $updatedBy
     * @return $this
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
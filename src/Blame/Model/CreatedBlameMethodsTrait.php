<?php

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Trait CreatedBlameMethodsTrait
 *
 * @author Kévin Tourret
 */
trait CreatedBlameMethodsTrait
{
    /**
     * @return object|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param object $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
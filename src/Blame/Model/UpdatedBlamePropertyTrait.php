<?php

namespace DrosalysWeb\ObjectExtensions\Blame\Model;

/**
 * Trait UpdatedBlamePropertyTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedBlamePropertyTrait
{
    /**
     * @var object|null
     */
    protected $updatedBy;
}
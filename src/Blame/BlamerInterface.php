<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Blame;

use DrosalysWeb\ObjectExtensions\Blame\Model\CreatedBlameInterface;
use DrosalysWeb\ObjectExtensions\Blame\Model\UpdatedBlameInterface;

/**
 * Interface BlamerInterface
 *
 * @author Benjamin Georgeault
 */
interface BlamerInterface
{
    /**
     * @param CreatedBlameInterface $blameObject
     */
    public function applyCreatedBy(CreatedBlameInterface $blameObject): void;

    /**
     * @param UpdatedBlameInterface $blameObject
     */
    public function applyUpdatedBy(UpdatedBlameInterface $blameObject): void;

    /**
     * Check if the given object is supported by this Blamer for the logged User.
     *
     * @param string $blameClass
     * @return bool
     */
    public function support(string $blameClass): bool;
}

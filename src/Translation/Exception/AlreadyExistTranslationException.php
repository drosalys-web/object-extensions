<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Translation\Exception;

use DrosalysWeb\ObjectExtensions\Translation\Model\TranslatableInterface;

/**
 * Class AlreadyExistTranslationException
 *
 * @author Benjamin Georgeault
 */
class AlreadyExistTranslationException extends \RuntimeException
{
    /**
     * @var TranslatableInterface
     */
    private $translatable;

    /**
     * @var string
     */
    private $locale;

    /**
     * AlreadyExistTranslationException constructor.
     * @param TranslatableInterface $translatable
     * @param string $locale
     */
    public function __construct(TranslatableInterface $translatable, string $locale)
    {
        $this->translatable = $translatable;
        $this->locale = $locale;

        parent::__construct(sprintf('Translation for locale "%s" already exist.', $locale));
    }
}

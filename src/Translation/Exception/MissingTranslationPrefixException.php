<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Translation\Exception;

/**
 * Class MissingTranslationPrefixException
 *
 * @author Benjamin Georgeault
 */
class MissingTranslationPrefixException extends \LogicException
{
    /**
     * MissingTranslationPrefixException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        parent::__construct(sprintf('The class "%s" missing Translation prefix directory.', $class));
    }
}

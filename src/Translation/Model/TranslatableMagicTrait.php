<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Translation\Model;

use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Trait TranslatableMagicTrait
 *
 * @author Benjamin Georgeault
 */
trait TranslatableMagicTrait
{
    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call(string $name, array $arguments)
    {
        if (preg_match('/^((get)|(is))(.*)$/', $name, $matches)) {
            return $this->doGet($this->doGetTranslationForMagic(0, $arguments), end($matches));
        }

        if (preg_match('/^(set)(.*)$/', $name, $matches)) {
            if (0 === count($arguments)) {
                throw new InvalidArgumentException('Missing value to set.');
            }

            return $this->doSet($this->doGetTranslationForMagic(1, $arguments), $matches[2], $arguments[0]);
        }

        if (0 === count($arguments)) {
            $translation = $this->doGetTranslationForMagic(0, $arguments);
            if ($this->getPropertyAccessor()->isReadable($translation, $name)) {
                return $this->doGet($translation, $name);
            }
        }

        throw new \BadMethodCallException(sprintf('Method "%s" not implemented.', $name));
    }

    /**
     * @param TranslationInterface $translation
     * @param string $name
     * @return mixed|null
     */
    private function doGet(TranslationInterface $translation, string $name)
    {
        if (!$this->getPropertyAccessor()->isReadable($translation, $name)) {
            throw new \BadMethodCallException(sprintf('Getter for "%s" not implemented.', $name));
        }

        return $this->getPropertyAccessor()->getValue($translation, $name);
    }

    /**
     * @param TranslationInterface $translation
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    private function doSet(TranslationInterface $translation, string $name, $value)
    {
        if (!$this->getPropertyAccessor()->isWritable($translation, $name)) {
            throw new \BadMethodCallException(sprintf('Setter for "%s" not implemented.', $name));
        }

        $this->getPropertyAccessor()->setValue($translation, $name, $value);

        return $this;
    }

    /**
     * @param int $fallbackIndex
     * @param array $arguments
     * @return TranslationInterface
     */
    private function doGetTranslationForMagic(int $fallbackIndex, array $arguments): TranslationInterface
    {
        if (array_key_exists($fallbackIndex, $arguments)) {
            $fallback = $arguments[$fallbackIndex];
        } else {
            $fallback = $this->getFallback();
        }

        if (null === $fallback) {
            throw new RuntimeException('Fallback not given.');
        }

        return $this->getTranslation($fallback);
    }

    /**
     * @return PropertyAccessor
     */
    private function getPropertyAccessor(): PropertyAccessor
    {
        return $this->propertyAccessor ?? $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }
}

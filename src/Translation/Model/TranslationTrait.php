<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Translation\Model;

/**
 * Trait TranslationTrait
 *
 * @author Benjamin Georgeault
 * @see TranslationInterface
 */
trait TranslationTrait // implements TranslationInterface
{
    /**
     * @var string
     */
    protected $locale = 'en';

    /**
     * @var null|TranslatableInterface
     */
    protected $translatable;

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return null|TranslatableInterface
     */
    public function getTranslatable(): ?TranslatableInterface
    {
        return $this->translatable;
    }

    /**
     * @param TranslatableInterface $translatable
     * @return $this
     */
    public function setTranslatable(TranslatableInterface $translatable)
    {
        $this->translatable = $translatable;

        return $this;
    }
}

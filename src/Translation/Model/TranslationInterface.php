<?php

/*
 * This file is part of the drosalys-web/object-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Translation\Model;

/**
 * Interface TranslationInterface
 *
 * @author Benjamin Georgeault
 */
interface TranslationInterface
{
    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale);

    /**
     * @return null|TranslatableInterface
     */
    public function getTranslatable(): ?TranslatableInterface;

    /**
     * @param TranslatableInterface $translatable
     * @return $this
     */
    public function setTranslatable(TranslatableInterface $translatable);
}

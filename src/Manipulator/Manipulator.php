<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Manipulator;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

/**
 * Class Manipulator
 *
 * @author Benjamin Georgeault
 */
class Manipulator implements ManipulatorInterface
{
    /**
     * @var PropertyInfoExtractorInterface
     */
    private $extractor;

    /**
     * @var PropertyAccessorInterface
     */
    private $accessor;

    public function __construct(?PropertyInfoExtractorInterface $extractor = null, ?PropertyAccessorInterface $accessor = null)
    {
        $this->extractor = $extractor ?? new ReflectionExtractor();
        $this->accessor = $accessor ?? PropertyAccess::createPropertyAccessor();
    }

    public function updater($target, ...$sources)
    {
        if (!is_object($target)) {
            throw new \InvalidArgumentException('First argument must be an object.');
        }

        return $this->doUpdater($this->extractor->getProperties(get_class($target)), $target, 1, ...$sources);
    }

    public function selectedUpdater(array $props, $target, ...$sources)
    {
        if (!is_object($target)) {
            throw new \InvalidArgumentException('Second argument must be an object.');
        }

        return $this->doUpdater(
            array_intersect($this->extractor->getProperties(get_class($target)), $props),
            $target,
            2,
            ...$sources
        );
    }

    public function ignoredUpdater(array $ignoredProps, $target, ...$sources)
    {
        if (!is_object($target)) {
            throw new \InvalidArgumentException('Second argument must be an object.');
        }

        return $this->doUpdater(
            array_diff($this->extractor->getProperties(get_class($target)), $ignoredProps),
            $target,
            2,
            ...$sources
        );
    }

    // TODO use Symfony property accessor
    public function hydrate($target, string $property, $value)
    {
        if (!is_object($target)) {
            throw new \InvalidArgumentException('First argument must be an object.');
        }

        $refObj = new \ReflectionObject($target);

        try {
            $refProp = $refObj->getProperty($property);
        } catch (\ReflectionException $e) {
            throw new \InvalidArgumentException(sprintf(
                'Given property "%s" does not exist on class "%s".',
                $property,
                get_class($target)
            ));
        }

        $refProp->setAccessible(true);
        $refProp->setValue($target, $value);
        $refProp->setAccessible(false);

        return $target;
    }

    private function doUpdater(array $props, $target, int $sourceStartAt, ...$sources)
    {
        foreach ($props as $prop) {
            if ($this->accessor->isWritable($target, $prop)) {
                foreach ($sources as $i => $source) {
                    if (!is_object($source) && !is_array($source)) {
                        throw new \InvalidArgumentException(sprintf('Argument n°%d must be an object or an array.', $i+$sourceStartAt+1));
                    }

                    $sourceProp = is_array($source) ? sprintf('[%s]', $prop) : $prop;
                    $targetProp = is_array($target) ? sprintf('[%s]', $prop) : $prop;

                    if (is_array($source) && !array_key_exists($prop, $source)) {
                        continue;
                    }

                    if ($this->accessor->isReadable($source, $sourceProp)) {
                        $this->accessor->setValue(
                            $target,
                            $targetProp,
                            $this->accessor->getValue($source, $sourceProp)
                        );
                    }
                }
            }
        }

        return $target;
    }
}

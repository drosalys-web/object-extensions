<?php

namespace DrosalysWeb\ObjectExtensions\Timestamp\Model;

/**
 * Trait UpdatedTimestampMethodsTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedTimestampMethodsTrait
{

    /**
     * @return null|\DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime|null $updatedAt
     * @return $this
     */
    public function setUpdatedAt(?\DateTime $updatedAt)
    {
        if (null !== $updatedAt) {
            $this->updatedAt = $updatedAt;
        }

        return $this;
    }

}
<?php

/*
 * This file is part of the object-extensions package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\ObjectExtensions\Timestamp\Model;

/**
 * Interface UpdatedTimestampInterface
 *
 * @author Benjamin Georgeault
 */
interface UpdatedTimestampInterface
{
    /**
     * @return null|\DateTime
     */
    public function getUpdatedAt(): ?\DateTime;

    /**
     * @param \DateTime|null $updatedAt
     * @return $this
     */
    public function setUpdatedAt(?\DateTime $updatedAt);
}

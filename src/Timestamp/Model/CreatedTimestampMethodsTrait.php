<?php

namespace DrosalysWeb\ObjectExtensions\Timestamp\Model;

/**
 * Trait CreatedTimestampMethodsTrait
 *
 * @author Kévin Tourret
 */
trait CreatedTimestampMethodsTrait
{

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt ? : $this->createdAt = new \DateTime();
    }

    /**
     * @param \DateTime|null $createdAt
     * @return $this
     */
    public function setCreatedAt(?\DateTime $createdAt)
    {
        if (null !== $createdAt) {
            $this->createdAt = $createdAt;
        }

        return $this;
    }

}
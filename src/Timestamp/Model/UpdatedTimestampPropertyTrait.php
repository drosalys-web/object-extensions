<?php

namespace DrosalysWeb\ObjectExtensions\Timestamp\Model;

/**
 * Trait UpdatedTimestampPropertyTrait
 *
 * @author Kévin Tourret
 */
trait UpdatedTimestampPropertyTrait
{

    /**
     * @var \DateTime|null
     */
    protected $updatedAt;

}
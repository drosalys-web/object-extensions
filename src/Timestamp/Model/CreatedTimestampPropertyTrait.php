<?php

namespace DrosalysWeb\ObjectExtensions\Timestamp\Model;

/**
 * Trait CreatedTimestampPropertyTrait
 *
 * @author Kévin Tourret
 */
trait CreatedTimestampPropertyTrait
{
    /**
     * @var \DateTime|null
     */
    protected $createdAt;
}
Object Extensions
=================

A library to extend Object capabilities.

## Install

```
composer require drosalys-web/object-extensions
```

### Symfony integration (^4.2|^5.0)

Flex not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    DrosalysWeb\ObjectExtensions\Bridge\Symfony\DrosalysWebObjectExtensionsBundle::class => ['all' => true],
    //...
];
```

- Create a new `config\packages\drosalys_object_extensions.yaml` file, you will config the bundle here.
```yaml
drosalys_object_extensions: ~
```

- That all for default configuration.

## Documentations

Here all capabilities documentations:
- [Blame](doc/blame.md): Allow an object to have a User referenced for creation and update on the object.
- [Hierarchy](doc/hierarchy.md): Allow to create a hierarchy for a given class on itself (and children classes).
- [Slug](doc/slug.md): Allow an object to have a normalize slug. Useful for url's path.
- [Timestamp](doc/timestamp.md): Same as Blame but for time.
- [Translation](doc/translation.md): Allow an object to have translation attributes.

## TODO

- Better documentation.
- Flex recipe for Symfony.
- Add example for standalone usage.

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
